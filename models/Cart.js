const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    ref: 'User',
    required: [true, 'User ID is required!'],
  },
  email: {
    type : String,
    required : [true, "Email is required!"]
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, 'Product ID is required!']
      },
      productName : {
        type : String,
        required: [true, "productId is required!"]
      },
      price: {
        type: Number,
        required: [true, 'Price of Product is required!']
      },
      quantity: {
        type: Number,
        required: [true, 'Quantity of product is required!']
      }
    },
  ],
});

module.exports = mongoose.model('Cart', cartSchema);
