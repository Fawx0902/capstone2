const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserControllers.js')
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth


// Register a new User
router.post('/register', (request, response) => {
    UserController.registerUser(request, response);
})

// Login User
router.post('/login', (request, response) => {
    UserController.loginUser(request, response);
})

// Retrieve User Details
router.post('/details', verify, (request, response) => {
	UserController.getUserProfile(request.body).then((result => {
        response.send(result)
    }));
})

// Retrieve all User Details (Admin Only)
router.get('/alldetails', verify, verifyAdmin, (request, response) => {
    UserController.getAllUserDetails(request, response);
})

// Set another user as Admin (Admin Only)
router.put('/setadmin', verify, verifyAdmin, (request, response) => {
    UserController.permitAdmin(request, response);
})

// Change user password
router.put('/changePassword', (request, response) => {
    UserController.changeUserPass(request, response)
})

module.exports = router;