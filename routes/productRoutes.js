const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductControllers.js')
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth

// Create a new Product
router.post('/register', verify, verifyAdmin, (request, response) => {
    ProductController.registerProduct(request.body).then((result => {
        response.send(result);
    }))
})

// Retrieve all Products
router.get('/all', (request, response) => {
    ProductController.getAllProducts(request, response)
})

// Retrieve all active Products
router.get('/', (request, response) => {
    ProductController.getAllActiveProducts(request, response)
})

// Retrieve Single Product
router.get('/:id', (request, response) => {
    ProductController.getProduct(request, response)
})

// Retrieve Single Product by Product Name
router.post('/search', (request, response) => {
    ProductController.getProductByName(request, response)
})


// Update Product Information (Admin)
router.put('/:id', verify, verifyAdmin, (request, response) => {
    ProductController.updateProduct(request, response)
})

// Archive Product (Admin)
router.put('/:id/archive', verify, verifyAdmin, (request, response) => {
    ProductController.archiveProduct(request, response)
})

// Activate Product (Admin)
router.put('/:id/activate', verify, verifyAdmin, (request, response) => {
    ProductController.activateProduct(request, response)
})

module.exports = router;