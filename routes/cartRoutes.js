const express = require('express');
const router = express.Router();
const CartController = require('../controllers/CartControllers.js')
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth

// Add Product to Cart (If Product is Active)
router.post('/add-product', verify, (request, response) => {
    CartController.addToCart(request, response);
})

// Edit Product Quantity of Cart
router.put('/edit-quantity', verify, (request, response) => {
    CartController.editQuantityCart(request, response)
})

// Remove Product from Cart
router.delete('/remove-product', verify, (request, response) => {
    CartController.removeProductCart(request, response)
})

// Show Subtotal of Item in Cart
router.post('/subtotal-item', verify, (request, response) => {
    CartController.showSubTotalProduct(request, response);
})

// Show Total Price of Items in Cart
router.get('/total-cart', verify, (request, response) => {
    CartController.showTotalCart(request, response);
})

module.exports = router;