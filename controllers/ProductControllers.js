const Product = require('../models/Product.js');


module.exports.registerProduct = (request_body) => {

    let new_product = new Product ({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    });

    return new_product.save().then((registered_product, error) => {
        if (error){
            return {
                message: error.message
            }
        }

        return {
            message: 'Successfully registered a new product!',
            data: registered_product
        }
    }).catch(error => console.log(error))
}

module.exports.getAllProducts = (request, response) => {
    return Product.find({}).then(result => {
        return response.send(result);
    })
}


module.exports.getAllActiveProducts = (request, response) => {
    return Product.find({isActive: true}).then(result => {
        return response.send(result);
    })
}

module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}


module.exports.getProductByName = async (request, response) => {
    const productName = request.body.productName;

    return Product.find({ name: {$regex: productName, $options: 'i'}}).then(result => {
        return response.send(result);
    }).catch(error => response.send({
        message: error.message
    }))
}


module.exports.updateProduct = (request, response) => {
    let updated_product_details = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    }

    return Product.findByIdAndUpdate(request.params.id, updated_product_details).then(() => {
        return response.send({
            message: 'Product has been updated successfully.'
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while updating the product.',
            error: error.message
        });
    })
}


module.exports.archiveProduct = (request, response) => {
    return Product.findByIdAndUpdate(request.params.id, {isActive: false}).then(() => {
        return response.send({
            message: 'Product has been archived successfully.'
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while archiving the product.',
            error: error.message
        });
    })
}


module.exports.activateProduct = (request, response) => {
    return Product.findByIdAndUpdate(request.params.id, {isActive: true}).then(() => {
        return response.send({
            message: 'Product has been activated successfully.'
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while activating the product.',
            error: error.message
        });
    })
}