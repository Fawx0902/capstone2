const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');

module.exports.addToCart = async (request, response) => {
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }
    
    const product = await Product.findOne({_id: request.body.productId, isActive: true}).select('name price')

    if (!product) {
        return response.send('Product is unavailable.')
    }

    const doesCartExist = await Cart.findOne({userId: request.user.id})

    if (doesCartExist) {
        const existingProducts = doesCartExist.products.findIndex(item => item.productId === request.body.productId)

        if (existingProducts !== -1) {
            return response.send({
                message: 'Item already exists in Cart!'
            })
        } else {
            doesCartExist.products.push({
                productName: product.name,
                productId: request.body.productId,
                price: product.price,
                quantity: request.body.quantity
            })

            return doesCartExist.save().then((update_cart, error) => {
                if (error){
                    return response.send({
                        message: error.message
                    })
                }
                
                return response.send({
                    message: 'Added item to cart!',
                    data: update_cart,
                })
            })
        }
    } else {
        let new_cart = new Cart ({
            userId: request.user.id,
            products: [{
                productName: product.name,
                productId: request.body.productId,
                price: product.price,
                quantity: request.body.quantity
            }],
        })

        return new_cart.save().then((register_cart, error) => {
            if (error){
                return response.send({
                    message: error.message
                })
            }
            
            return response.send({
                message: 'Added item to cart!',
                data: register_cart,
            })
        })
    }
}

module.exports.editQuantityCart = async (request, response) => {
    if (request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    return Cart.findOneAndUpdate(
        { userId: request.user.id, 'products.productId': request.body.productId },
        { $set: {'products.$.quantity' : request.body.quantity} }
    ).then(() => {
        return response.send({
            message: 'Product Quantity updated succesfully!',
        })
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while updating product quantity in Cart.',
            error: error.message
        });
    })
}

module.exports.removeProductCart = (request, response) => {
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    return Cart.findOneAndUpdate(
        { userId: request.user.id },
        { $pull: { products: { productId: request.body.productId } } },
        { safe: true, multi: false, new: true }
    ).then((updatedCart) => {
        return response.send({
            message: 'Product successfully removed from Cart!',
            updatedCart: updatedCart
        })
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while deleting a product in your Cart.',
            error: error.message
        });
    })
}

module.exports.showSubTotalProduct = async (request, response) => {
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    const cart = await Cart.findOne({ userId: request.user.id });

    if (!cart) {
            return response.send('Cart not found.');
    }

    const cartItem = cart.products.find(item => item.productId === request.body.productId);

    if (!cartItem) {
        return response.send('Cart item not found.');
    }

    const subTotal = cartItem.quantity * cartItem.price

    return response.send({
        message: 'Cart item subtotal calculated successfully',
        cartItem: cartItem,
        cartItemSubtotal: subTotal
    });
}

module.exports.showTotalCart = async (request, response) => {
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    const cart = await Cart.findOne({ userId: request.user.id });

    if (!cart) {
            return response.send('Cart not found.');
    }

    let total = 0;

    for (const item of cart.products) {
        total += item.quantity * item.price
    }

    return response.send({
        message: 'Cart total calculated successfully',
        cart: cart,
        cartTotal: total
    });
}