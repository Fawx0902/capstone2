const Order = require('../models/Order.js');
const Product = require('../models/Product.js');

module.exports.checkoutOrder = async (request, response) => {
    // Blocks this function if user is admin.
    if(request.user.isAdmin){
        return response.send('Action Forbidden')
    }

    const product = await Product.findOne({_id: request.body.productId, isActive: true}).select('name price')

    if (!product) {
        return response.send('Product is unavailable.')
    }

    const currentDate = new Date();

    let new_Order = new Order({
        userId: request.user.id,
        email: request.user.email,
        products: [{
            productName: product.name,
            productId: request.body.productId,
            price: product.price,
            quantity: request.body.quantity,
        }],
        totalAmount: product.price * request.body.quantity,
        purchasedOn: currentDate
    })

    return new_Order.save().then((register_order, error) => {
        if (error){
            return response.send({
                message: error.message
            })
        }
        
        return response.send({
            message: 'Successfully checkout Order!',
            data: register_order
        })
    })
}

module.exports.viewOrders = (request, response) => {
    return Order.find({userId: request.user.id}).then(result => {
        return response.send(result);
    })
}

module.exports.viewAllOrders = (request, response) => {
    return Order.find({}).then(result => {
        return response.send(result);
    })
}