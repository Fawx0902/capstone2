const User = require('../models/User.js');
const bcrypt = require('bcrypt')
const auth = require('../auth.js');

module.exports.registerUser = async (request, response) => {

    const isDuplicateEmail = await User.findOne({email: request.body.email}).select('email')
    
    if (isDuplicateEmail.email == request.body.email) {
        return response.send({
            message: 'Email already taken!'
        })
    }

    let new_user = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		mobileNo: request.body.mobileNo,
		password: bcrypt.hashSync(request.body.password, 10)
	});

    return new_user.save().then((error) => {
        if (error){
            return response.send({
                message: error.message
            })
        }

        return response.send({
            message: 'Successfully registered a user!',
        })
    })
}

module.exports.loginUser = (request, response) => {
    return User.findOne({email: request.body.email}).then((result) => {
        if (result == null){
            return response.send({
                message: "The user isn't registered yet."
            })
        }

        // If a user was found with an existing email, then check if the password of that user matched the input from the request body
        const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

        if(isPasswordCorrect){
            // If the password comparison returns true, then respond with the newly generated JWT access token.
            return response.send({accessToken: auth.createAccessToken(result), userId: result._id});
        } else {
            return response.send({
                message: "Your password is incorrect."
            })
        }
    })
}

module.exports.getUserProfile = (request_body) => {
    return User.findOne({_id: request_body.id}).then((user, error) => {
        if (error) {
        return { message: error.message };
    } 
        user.password = ""
        return user;
    }).catch(error => console.log(error))
}

module.exports.getAllUserDetails = (request, response) => {
    return User.find({}).then(result => {
        result.password = "";
        return response.send(result);
    })
}

module.exports.permitAdmin = (request, response) => {
    return User.findByIdAndUpdate(request.body.id, {isAdmin: true}).then(() => {
        return response.send({
            message: `Succesfully bestowed ${request.body.firstName} ${request.body.lastName} with Admin privileges.`
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occurred while granting someone admin.',
            error: error.message
        });
    })
}

module.exports.changeUserPass = async (request, response) => {
    const currentUser = await User.findById(request.body.id)

    const currentPassword = request.body.currentPass;
    const isPasswordMatch = await bcrypt.compare(currentPassword, currentUser.password);

    if (!isPasswordMatch){
        return response.send({
            message: 'Password does not match within the database.'
        })
    }

    const newPassword = request.body.newPass;
    const newPasswordRepeat = request.body.newPassRepeat;

    if (newPassword !== newPasswordRepeat) {
        return response.send({
            message: 'New password does not match.'
        })
    }

    return User.findByIdAndUpdate(request.body.id, {password: bcrypt.hashSync(newPassword, 10)}).then(() =>{
        return response.send({
            message: 'Password changed successfully!'
        });
    })
    .catch(error => {
        return response.send({
            message: 'An error occured while trying to change user password.',
            error: error.message
        });
    })
}